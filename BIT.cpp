#include <bits/stdc++.h>
using namespace std;
class BIT{
  private:
    vector <int> arr;
  public:
    //constructor
    BIT(vector<int> list){
      //initialize the bit on n+1 positions
      arr = vector<int>(list.size()+1,0);
      //full the array
      for (int i = 0; i < list.size(); i++) {
        arr[i+1] = list[i];
      }
      for (int j = 1; j < arr.size(); j++) {
        //binary number
        int idx2 = j + (j & -j);
        //add the sums of the numbers
        if (idx2 < arr.size()) {
				      arr[idx2] += arr[j];
        }
      }
    }
    //query 1
    int prefix_query(int idx) {
		  // Computes prefix sum of up to including the idx-th element
		  int result = 0;
      //init on one
      //got through the last one on the bynary form
		  for (++idx; idx > 0; idx -= idx & -idx) {
			   result += m_array[idx];
		  }
		  return result;
    }
    //query 2
    int range_query(int idx1,int idx2){
      int r = prefix_query(idx1)-prefix_query(idx2);
      return r;
    }

    //update
    void update(int index, int val){
      // Add a value to the idx-th element
      //go through the array on binary order
      for (++index; index < arr.size(); index += index & -index) {
        m_array[index] += val;
      }
    }
}
