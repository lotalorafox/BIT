import java.util.Arrays;
public class BIT{
  //cumulative array
  public int[] arr;
  public int[] ori;

  public BIT(int[] list){
    arr = new int[list.length+1];
    ori = list;
    //full the array
    for (int i=0;i<list.length ;i++ ) {
      arr[i+1]=list[i];
    }
    //bynary form
    for (int j=1;j<arr.length ;j++ ) {
      //binary number
      int idx2 = j + (j & -j);
      //add the sums of the numbers
      if (idx2 < arr.length) {
            arr[idx2] += arr[j];
      }
    }
  }
  //update
  void update(int idx, int val){
    // Add a value to the idx-th element
    //go through the array on binary order
    ori[idx] += val;
    for (++idx; idx < arr.length; idx += idx & -idx) {
      arr[idx] += val;
    }
  }
  void replace(int idx,int val){
    int i = idx;
    int r = val-ori[idx];
    update(i,r);
  }
  //query 1
  int prefix_query(int idx) {
    // Computes prefix sum of up to including the idx-th element
    int result = 0;
    //init on one
    //got through the last one on the bynary form
    for (++idx; idx > 0; idx -= idx & -idx) {
       result += arr[idx];
    }
    return result;
  }
  //query 2
  int range_query(int idx1,int idx2){
    int r = prefix_query(idx1)-prefix_query(idx2);
    return r;
  }
  void print(){
    System.out.println("Values: " + Arrays.toString(ori));
    System.out.println("Sums: " + Arrays.toString(arr));
  }

}
