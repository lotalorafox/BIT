import java.util.*;
public class prove{
  public static void main(String[] args) {
    int[] a = { 1, 7, 3, 0, 5, 8, 3, 2, 6, 2, 1, 1, 4, 5 };
    BIT arr = new BIT(a);
    System.out.println(Arrays.toString(a));
    arr.print();
    System.out.println("Prefix sum of first 13 elements: "+ arr.prefix_query(12));
    System.out.println("Prefix sum of first 1 elements: "+ arr.prefix_query(0));
    System.out.println("Prefix sum of first 2 elements: "+ arr.prefix_query(1));
    System.out.println("Prefix sum of first 3 elements: "+ arr.prefix_query(2));
    System.out.println("---------------------------Update--------------------");
    arr.print();
    arr.update(1, 5);
    arr.print();
    System.out.println(Arrays.toString(a));
    System.out.println("Prefix sum of first 2 elements: "+ arr.prefix_query(1));
    System.out.println("Prefix sum of first 3 elements: "+ arr.prefix_query(2));
    System.out.println("------------------------Replace---------------------");
    arr.replace(1, 5);
    arr.print();
    System.out.println("Prefix sum of first 2 elements: "+ arr.prefix_query(1));
    System.out.println("Prefix sum of first 3 elements: "+ arr.prefix_query(2));
  }
}
